<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


use  App\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
         $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
         ]);

         try {
             $plainPassword = $request->password;

             $user              = new User;
             $user->name        = $request->name;
             $user->email       = $request->email;
             $user->password    = app('hash')->make($plainPassword);
             $user->save(); 
             //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);            
         } catch (Exception $e) {
             //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
         }
    }


    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        try {
            $credentials = $request->only(['email', 'password']);

            if (!$token = Auth::attempt($credentials)) {
                return response()->json(['message' => 'Unauthorized'], 401);
            } else {
                return $this->respondWithToken($token);
            }
        } catch (Exception $e) {
            return response()->json(['message' => 'User Invalid!'], 409);
        }
    }
}
