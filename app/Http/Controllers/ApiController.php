<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Carbon\Carbon;
use  App\model\Loan;
use  App\model\Payment;

class ApiController extends Controller
{
     function __construct()
    {
        $this->middleware('auth');
    }
    public function pay_loan(Request $request)
    {
        $this->validate($request, [
            'loanid' => 'required',
            'amount' => 'required',
         ]);

        try {
            $loanid = $request->loanid;
            $amount = $request->amount;
            
            $countpayment = Payment::where('loanid','=',$loanid)->count();
            $loan = Loan::select('paymentdate','payment_type')->where('id','=',$loanid)->first();
            $first_paymentdate = $loan->paymentdate; // First payment Date
            $type = $loan->payment_type; // Payment Type
            if ($countpayment == 0) {
               // First Payment
               $loanpayment             = new Payment;
               $loanpayment->loanid     = $loanid;
               $loanpayment->amount     = $amount;
               $loanpayment->save(); 
                return response()->json(['message' => 'First Payment Send'], 201);  
            } else {
                $payment = Payment::where('loanid','=',$loanid)->latest()->first(); 
                $created_datetime = $payment->created_at;
                $split_created = explode(' ',$created_datetime); 

                $date = explode('-',$split_created[0]); 

                $paymentdate = date("Y-m-d",strtotime($split_created[0]." +1 weeks"));

                // current date 
                $current_date_time = Carbon::now();
                $currentdate = $current_date_time->toDateString();
                

                if ($currentdate == $paymentdate ) {
                    $loanpayment             = new Payment;
                    $loanpayment->loanid     = $loanid;
                    $loanpayment->amount     = $amount;
                    $loanpayment->save(); 
                     return response()->json(['message' => 'Payment Send'], 201);  
                } else {
                    return response()->json(['message' => 'today is Not Payment Date'], 201);
                }
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }
}
