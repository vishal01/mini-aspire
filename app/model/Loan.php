<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $table = 'loans';

    protected $primaryKey = 'id';

     protected $fillable = [
        'amount',
        'loanterm',
        'paymentdate',
        'payment_type',
        'userid',
        'created_at',
        'updated_at'
    ];
}
