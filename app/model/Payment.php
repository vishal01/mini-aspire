<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $primaryKey = 'id';

     protected $fillable = [
        'loanid',
        'amount',
        'created_at',
        'updated_at'
    ];
}
